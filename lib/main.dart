import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'The Hello World App Of Destiny',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to my FIRST mobile app!'),
          backgroundColor: Colors.pinkAccent,
        ),
        body: Center(
          child: Text('Hello There.\n\n\t       :)'),
        ),
      ),
    );
  }
}

